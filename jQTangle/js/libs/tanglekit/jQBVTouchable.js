//
//  BVTouchable.js
//  ExplorableExplanations
//
//  Created by Bret Victor on 3/10/11.
//  (c) 2011 Bret Victor.  MIT open-source license.
//

( function() {

var jQBVTouchable = this.jQBVTouchable = function() {
	this.initialize.apply(this, arguments);
};

jQBVTouchable.prototype = {
	initialize: function (el, delegate) {
		// STAN: Added a $document member to this class for use later.
		//       This is so that we wrap document in jQuery once
		//       This might be a nice standard property to be available
		//       in almost every object used in tangle, since it is synonymous
		//       with mootools getDocument(), though it is only used in this object.
		this.$document = $(window.document);

		this.element = el;
		this.delegate = delegate;
		this.setTouchable(true);
	},

	//----------------------------------------------------------------------------------
	//
	//  touches
	//

	setTouchable: function (isTouchable) {
		this.$element = $(this.element);

		if (this.touchable === isTouchable) { return;}
		this.touchable = isTouchable;
		this.element.style.pointerEvents = (this.touchable || this.hoverable) ? "auto" : "none";

		if (isTouchable) {
			if (!this._mouseBound) {
				this._mouseBound = {
					mouseDown: this._mouseDown.bind(this),
					mouseMove: this._mouseMove.bind(this),
					mouseUp: this._mouseUp.bind(this),
					touchStart: this._touchStart.bind(this),
					touchMove: this._touchMove.bind(this),
					touchEnd: this._touchEnd.bind(this),
					touchCancel: this._touchCancel.bind(this)
				};
			}
			this.$element.on("mousedown", this._mouseBound.mouseDown);
			this.$element.on("touchstart", this._mouseBound.touchStart);
		}
		else {
			this.$element.off("mousedown");
			this.$element.off("touchstart");
		}
	},

	touchDidGoDown: function (touches) { this.delegate.touchDidGoDown(touches);},
	touchDidMove: function (touches) { this.delegate.touchDidMove(touches);},
	touchDidGoUp: function (touches) { this.delegate.touchDidGoUp(touches);},


	_mouseDown: function (event) {
		event.stopPropagation();
		event.preventDefault();

		// STAN: These events need to be attached to the document
		//       so that they are fired wherever the mouse moves.
		this.$document.on({
			mousemove: this._mouseBound.mouseMove,
			mouseup: this._mouseBound.mouseUp
		});
		this.touches = new BVTouches(event);
		this.touchDidGoDown(this.touches);
	},

	_mouseMove: function (event) {
		event.stopPropagation();
		event.preventDefault();
		this.touches.updateWithEvent(event);
		this.touchDidMove(this.touches);
	},

	_mouseUp: function (event) {
		event.stopPropagation();
		event.preventDefault();
		this.touches.updateWithEvent(event);
		this.touches.count = 0;
		this.touchDidGoUp(this.touches);

		delete this.touches;
		// STAN: Added fix here too - with newer 'off' function instead of 'unbind'
		this.$document.off({
			mousemove: this._mouseBound.mouseMove,
			mouseup: this._mouseBound.mouseUp
		});
	},

	_touchStart: function (event) {
		event.stopPropagation();
		event.preventDefault();
		if (this.touches || event.length > 1) { this._touchCancel(event); return;}  // only-single touch for now

		// STAN: Added fix for touch events too
		this.$document.on({
			touchmove: this._mouseBound.touchMove,
			touchend: this._mouseBound.touchEnd,
			touchcancel: this._mouseBound.touchCancel
		});

		this.touches = new BVTouches(event);
		this.touchDidGoDown(this.touches);
	},

	_touchMove: function (event) {
		event.stopPropagation();
		event.preventDefault();
		if (!this.touches) { return;}

		this.touches.updateWithEvent(event);
		this.touchDidMove(this.touches);
	},

	_touchEnd: function (event) {
		event.stopPropagation();
		event.preventDefault();
		if (!this.touches) { return;}

		this.touches.count = 0;
		this.touchDidGoUp(this.touches);

		delete this.touches;
		// STAN: And here
		this.$document.off({
			touchmove: this._mouseBound.touchMove,
			touchend: this._mouseBound.touchEnd,
			touchcancel: this._mouseBound.touchCancel
		});
	},

	_touchCancel: function (event) {
		this._touchEnd(event);
	}

};


//====================================================================================
//
//  BVTouches
//

var BVTouches = this.BVTouches = function() {
	this.initialize.apply(this, arguments);
};

BVTouches.prototype = {
	initialize: function (event) {
		this.globalPoint = { x:event.pageX, y:-event.pageY};
		this.translation = { x:0, y:0};
		this.deltaTranslation = { x:0, y:0};
		this.count = 1;
		this.event = event;
	},

	updateWithEvent: function (event) {
		var dx = event.pageX - this.globalPoint.x;  // todo, transform to local coordinate space?
		var dy = -event.pageY - this.globalPoint.y;
		this.translation.x += dx;
		this.translation.y += dy;
		this.deltaTranslation.x += dx;
		this.deltaTranslation.y += dy;
		this.globalPoint.x = event.pageX;
		this.globalPoint.y = -event.pageY;
		this.event = event;
	},

	resetDeltaTranslation: function () {
		this.deltaTranslation.x = 0;
		this.deltaTranslation.y = 0;
	}

};


//====================================================================================
})();
