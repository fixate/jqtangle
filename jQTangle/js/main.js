(function() {

	var element = document.getElementById("example-01"),
			a = document.getElementById("example-02"),
			b = document.getElementById("example-03"),
			c = document.getElementById("example-04"),
			d = document.getElementById("example-05"),
			e = document.getElementById("example-06");

	// TKAdjustableNumber
	// var tangle = new Tangle(element, {
	//     initialize: function () {
	//         this.cookies = 4;
	//         this.caloriesPerCookie = 50;
	//     },
	//     update: function () {
	//         this.calories = this.cookies * this.caloriesPerCookie;
	//     }
	// });

	// jQTKAdjustableNumber
	var tangle = new Tangle(element, {
		initialize: function () {
			this.cookies = 4;
			this.caloriesPerCookie = 50;
		},
		update: function () {
			this.calories = this.cookies * this.caloriesPerCookie;
		}
	});

	// jQTKToggle
	var tangle = new Tangle(a, {
			initialize: function () {
				this.cookies = 0;
				this.setting = '';
			},
			update: function () {
				this.setting = (this.cookies === 1) ? 'on' : 'off';
			}
	});

	// jQTKIf
	var tangle = new Tangle(b, {
			initialize: function () {
				this.iftest = 0;
			},
			update: function () {

			}
	});

	// jQTKNumberField
	var tangle = new Tangle(c, {
			initialize: function () {
				this.cookies = 1;
				this.calories = 50;
				this.caloriesPerCookie = 50
			},
			update: function () {
				this.calories = this.caloriesPerCookie * this.cookies;
			}
	});

	// jQTKSwitch
	var tangle = new Tangle(d, {
			initialize: function () {
				this['show-item'] = 3;
			},
			update: function () {

			}
	});

	// jQTKSwitchPositiveNegative
	var tangle = new Tangle(e, {
			initialize: function () {
				this['show-item'] = -1;
			},
			update: function () {

			}
	});

})();